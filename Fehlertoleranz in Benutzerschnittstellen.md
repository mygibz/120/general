# Fehlertoleranz

## Definition

> Der Nutzer soll sein Ziel trotz fehlerhafter Eingabe mit minimalem
> Korrekturaufwand erreichen können.

## Erklärung

Fehler teilen sich auf in Vermeidbare Fehler, bekannte, nicht vermeidbare Fehler, nicht erwartete Fehler

Die Software soll die Fehler selbst erkennen und wenn möglich korrigieren. Bei Fehlermeldungen sind Fachbegriffe zu vermeiden.

## Beispiele

### Positiv

#### 1 - "Did you mean this?"

![](https://i.ibb.co/9YqqPXZ/Microsoft-Teams-image.png)

Dem Nutzer wird eine mögliche Korrektur der Eingabe vorgeschlagen.

#### 2 - Website Weiterleitung 

![](https://www.welaunch.io/en/wp-content/uploads/sites/8/2020/07/country-redirect.png)

Wenn man z.B. *aus versehen* auf amazon.ch geht, wird man auf amazon.de weitergeleitet.

#### 3 - Did you really want to close?

Bein schliessen von Word, wird man gefragt, ob man wirklich speichern will und nicht einfach nur schliessen will.

### Negativ

#### 1 - Telefonnummer eingabe

Software erkennt '0' am beginn der Eingabe, zwingt den Nutzer aber dazu den Fehler zu korrigieren, anstadt ihn selbst zu korigieren.

#### 2 - Dateiformat

![](https://userinterfacedesign.ch/wp-content/uploads/2017/04/fehlertoleranz.png)

Der Nutzer wird einfach darauf hingewiesen, dass man nur JPEG, PNG und GIF dateien hochladen kann, nutzer muss das Bild manuell convertieren und dann von vorne beginnen.

