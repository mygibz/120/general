# Informationen zur Theorieprüfung

Die Theorieprüfung im Modul 120 findet nach ungefähr der Hälfte der Unterrichtszeit statt und dauert ca. 90 Minuten. Die Anteil an der Semesternote beträgt 30%.

- Erlaubte Hilfsmittel: keine
- Prüfungsform: Theorieprüfung (analog, d.h. ohne BYOD)

Unmittelbar vor der Prüfung haben Sie die Möglichkeit offene Fragen zu stellen. So können allfällige Unklarheiten oder Unsicherheiten nochmals besprochen werden.

## Lernziele

Die nachfolgenden Lernziele gelten für die Theorieprüfung im Modul 120.

### Software-Ergonomie

- [x] Sie kennen die sieben Grundsätze der Dialoggestaltung gemäss ISO 9241 Teil 110.

- [x] Sie können die wesentlichen Grundlagen der sieben Grundsätze der Dialoggestaltung gemäss ISO 9241 Teil 110 erklären.
- [x] Sie können die Bedeutung der sieben Grundsätze der Dialoggestaltung gemäss ISO 9241 Teil 110 anhand konkreter Situationen und Beispiele verdeutlichen.
- [x] Sie können den Begriff _Style Guide_ im Kontext der Gestaltung von Benutzungsschnittstellen erklären.
- [x] Sie können den Nutzen und die Funktion von Style Guides für die Erstellung von Benutzungsschnittstellen erklären.
- [x] Sie können wesentliche Informationen zur Gestaltung von Benutzungsschnittstellen aus Style Guides (und referenzierten Quellen) identifizieren und umsetzen.

### Data Binding

- [x] Sie kennen die relevanten, technischen Grundlagen zur Implementierung von Applikationen mit grafischen Benutzungsschnittstellen in C#, WPF und Data Binding. Dazu zählen insbesondere die Begriffe/Konzepte *Source*, *Target*, *Mode* (`OneWay`, `TwoWay`, `OneWayToSource`, `OneTime`) und *UpdateSourceTrigger* (`LostFocus`, `PropertyChanged`, `Explicit`, `Default`).
- [x] Sie können das Konzept des (hierarchischen) `DataContext` erklären und korrekt anwenden.
- [ ] Sie können das Data Binding in C#/WPF für verschiedene UI Elemente gezielt und effizient implementieren.
- [x] Sie können den Nutzen und die grundlegende Funktionsweise von *Commands* in C#/WPF erklären und dabei zwischen System-Commands und selber implementierten Commands unterscheiden.

### MVVM (Model, View, ViewModel)

- [x] Sie kennen die Grundidee bzw. das Ziel, weshalb Applikationen (z.B.) mit C# und WPF gemäss dem MVVM Entwurfsmuster implementiert werden.
- [x] Sie kennen Model, View und ViewModel als sie drei wesentlichen Komponenten zur Implementation einer Applikation nach dem MVVM Entwurfsmuster.
- [x] Sie können die Zuständigkeiten der drei wesentlichen Komponenten in MVVM sowie deren Interaktion in eigenen Worten erklären.
- [ ] Sie können wesentliche Vor- und Nachteile der Anwendung des MVVM Entwurfsmuster aufzeigen.